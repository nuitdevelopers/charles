import { Permission} from "@/core/user/Permission"
import CommonObject from "@/core/types/CommonObject"
import CommonDTO from "@/core/types/CommonDTO"

export class User extends CommonObject {
    userName = ""
    email = ""
    password = ""
    roleIds = new Array<string>()
    permissions = new Array<Permission>()
}

export interface UserDTO extends CommonDTO {
    userName: string
    email: string
    password: string
    roleIds: string[]
    permissions: Permission[]
}