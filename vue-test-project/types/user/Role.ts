import Translation from "@/core/types/Translation"
import CommonObject from "@/core/types/CommonObject"
import CommonDTO from "@/core/types/CommonDTO"

export class Role extends CommonObject {
    name: Translation = {
        en: "",
    }
    desc: Translation = {
        en: "",
    }
    note = ""
    permissionIds = new Array<string>
}


export interface RoleDTO extends CommonDTO {
    name: Translation
    desc: Translation
    note: string
    permissionIds: string[]
}