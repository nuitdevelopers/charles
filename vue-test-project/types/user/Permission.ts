import Translation from "@/core/types/Translation"
import CommonObject from "@/core/types/CommonObject"
import CommonDTO from "@/core/types/CommonDTO"

export class Permission extends CommonObject {
    name: Translation = {
        en: "",
    }
    desc: Translation = {
        en: "",
    }
    note = ""
}

export interface PermissionDTO extends CommonDTO {
    name: Translation
    desc: Translation
    note: string
}