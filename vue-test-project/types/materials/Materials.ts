import Guid from "@/core/types/Guid";
import Translation from "@/core/types/Translation";


export class Material implements Guid {
    rawMaterialId = ""
    id = ""
    name: Translation = {
        en: "",
    }
    note: Translation = {
        en: "",
    }
    externalId = ""
    coatingId = ""
    color = {
        frontId: "",
        backId: "",
    }
    thickness = 0
    width = 0
    hasFoil = false
    isPerforated = false
    coilHandling = {
        straightening: new Array<{min: number, max: number}>(),
        slitting: {
            kerf: 0,
            overlap: 0,
        },
    }
    createdAt: Date = new Date()
    updatedAt: Date = new Date()
    updatedBy = ""
    customData: Record<string , unknown> = {}
    freeCrossSectionPercent = 0
}

export interface MaterialDTO extends Guid {
    rawMaterialId: string
    id: string
    name: Translation
    note: Translation
    externalId: string
}
