import Guid from "@/core/types/Guid"

export class Coil implements Guid {
    id = ""
    name = ""
    externalId = ""
    materialId = ""
    note = ""
    batchNumber = ""
    stockId = ""
    width = 0
    handling = {
        straightenings : new Array<{ min: number; max: number}>(),
        slitting: {
            kerf: 0,
            overlap: 0,
        },
    }
    thickness = 0
    weight = {
        initial: 0,
        remaining: 0,
    }

    length = {
        initial: 0,
        used: 0,
        remaining: 0,
    }

    createdAt: Date = new Date()
    updatedAt: Date = new Date()
    updatedBy: Date = new Date()
}

export interface CoilDTO extends Guid {
    id: string
    name: string
    externalId: string
    materialId: string
    note: string
    batchNumber: string
    stockId: string
    width: number
    handling: {
        straightenings: { min: number, max: number}[]
        slitting: { kerf: number; overlap: number }
    }
    thickness: number
    weight: {initial: number; remaining: number}
    length: { initial: number; used: number; remaining: number}
    createdAt: string
    updatedAt: string
    lastUsed: string
}