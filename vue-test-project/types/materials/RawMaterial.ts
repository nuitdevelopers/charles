import Guid from "@/core/types/Guid"
import Translation from "@/core/types/Translation"

export class RawMaterial implements Guid {
    id = ""
    name: Translation = {
        en: "",
    }
    density = 0
    customData: Record<string, unknown> = {}
    updatedAt: Date = new Date()
    createdAt: Date = new Date()
    updatedBy = ""
}

export interface RawMaterialDTO extends Guid {
    id: string
    name: Translation
    density: number
    customData: Record<string, unknown>
    createdAt: string
    updatedAt: string
    updatedBy: string
}