import Guid from "@/core/types/Guid";
import Translation from "@/core/types/Translation"

export class Coatings implements Guid {
    name: Translation = {
        en: "",

    }
    id = ""
    note: Translation = {
        en: "",
    }

    createdAt: Date = new Date()
    updatedAt: Date = new Date()
    updatedBy = ""
}

export interface CoatingDTO extends Guid {
    name: Translation
    id: string
    note: Translation
    createdAt: string
    updatedAt: string
    updatedBy: string
}