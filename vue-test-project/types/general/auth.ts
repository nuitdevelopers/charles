import Guid from "@/core/types/Guid";

export interface Auth extends Guid  {
    id: string
    email: string
    password?: string
    token: string

}

export interface AuthDTO extends Guid {
    id:string
    username: string
    token: string
    permissions: { id: string }[]
}