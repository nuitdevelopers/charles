export interface CoilPlaceConfig {
    label: string
    line: number
    pos: number
    type: string
}