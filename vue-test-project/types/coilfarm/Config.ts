import Guid from "@/core/types/Guid"
import { CoilPlaceConfig } from "@/types/coilfarm/CoilPlaceConfig"

export interface Config extends Guid {
    id: string
    coilFarmId: string
    coilPlaces: CoilPlaceConfig[]
    createdAt: Date
    createdBy: string
}

export interface ConfigDTO extends Guid {
    id: string
    coilFarmId: string
    coilPlaces: CoilPlaceConfig[]
    createdAt: string
    createdBy: string
}