import Guid from "@/core/types/Guid"

export class CoilPlace implements Guid {
    id = ""
    coilFarmId = ""
    coilId = ""
    onPlaceAt: Date = new Date()
    type = 0
    label = ""
    line = 0
    pos = 0
    disabled = false
    createdAt: Date = new Date()
    updatedAt: Date = new Date()
    updatedBy = ""
}


export interface CoilPlaceDTO extends Guid {
    id:string
    coilFarmId: string
    coilId: string
    onPlaceAt: string
    type: number
    label: string
    line: number
    pos: number
    disabled: boolean
    createdAt: string
    updatedAt: string
    updatedBy: string
}