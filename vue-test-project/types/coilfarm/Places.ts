import { CoilPlace, CoilPlaceDTO } from "@/types/coilfarm/CoilPlace";

export interface Places {
    buffers: CoilPlace[]
}

export interface PlaceDTO {
    buffers: CoilPlaceDTO[]
}