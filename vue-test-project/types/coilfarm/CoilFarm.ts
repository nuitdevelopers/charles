import Guid from "@/core/types/Guid";
import Translation from "@/core/types/Translation"
import { CoilPlace, CoilPlaceDTO} from "@/types/coilfarm/CoilPlace"

export class CoilFarm implements Guid {
    id = ""
    name: Translation = {
        en: "",
    }
    type : ""
    places = {
        buffers: new Array<CoilPlace>(),
        coils: new Array<CoilPlace>()
    }
    deletedAt: Date = new Date()
    createdAt: Date = new Date()
    updatedAt: Date = new Date()
    updatedBy = ""
}


export interface CoilFarmDTO extends Guid {
    id:string
    name:Translation
    type:string
    places?: { buffers?: CoilPlaceDTO[]; coils?: CoilPlaceDTO[] }
    deletedAt: string
    createdAt: string
    updatedAt: string
    updatedBy: string
}