import Guid from "@/core/types/Guid"
import Translation from "@/core/types/Translation"

export interface Message extends Guid {
    id:string
    definitionId: string
    definition: MessageDefinition
    data: MessageData
    resetAt: Date
    resetBy: string
    createdAt: Date
}

export interface MessageDTO extends Guid {
    id:string
    definitionId: string
    definition: MessageDefinition
    data: MessageData
    resetAt: string
    resetBy: string
    createdAt: string
}


export interface MessageDefinition extends Guid {
    messageBody: string
}