import { defineStore} from "pinia"
import { ref, computed} from "vue"

export const useCounterStore = defineStore("counter", () => {
    const count = ref(0)

    //getters
    const isEven = computed(() => {
        return count.value % 2 == 0;
    })

    const messageIfEven = computed(() => {
        return (message:string) => {
            if(!isEven.value) return
            return message
        }
    })

    //actions
    function increment(): void {
        count.value++;
    }

    function clear(): void {
        count.value = 0
    }

    return { count, increment, clear }
})


