import { defineStore } from 'pinia'
import { CoilFarm, CoilFarmDTO } from '@/types/coilfarm/CoilFarm'
import { CoilPlace, CoilPlaceDTO } from '@/types/coilfarm/CoilPlace'
import { ref } from 'vue'
import COMMON_API from '@/core/api/CommonApi'
import DATE from '@/core/helper/date'
import Update from '@/core/api/Update'

export const useCoilFarmStore = defineStore('Coilfarmstore', () => {
  const coilFarmArray = ref<CoilFarm[]>([])

  function toCoilPlaceDTOs(places: CoilPlace[]): CoilPlaceDTO[] {
    const coilPlaceDTOs: CoilPlaceDTO[] = []

    for (const entity of places) {
      coilPlaceDTOs.push({
        ...entity,
        onPlaceAt: DATE.toDTO(entity.onPlaceAt),
        createdAt: DATE.toDTO(entity.createdAt),
        updatedAt: DATE.toDTO(entity.updatedAt),
      })
    }

    return coilPlaceDTOs
  }

  function fromCoilPlaceDTOs(places: CoilPlaceDTO[]): CoilPlace[] {
    const coilPlaces: CoilPlace[] = []

    if (places !== null) {
      for (const entity of places) {
        coilPlaces.push({
          ...entity,
          onPlaceAt: DATE.fromDTO(entity.onPlaceAt),
          createdAt: DATE.fromDTO(entity.createdAt),
          updatedAt: DATE.fromDTO(entity.updatedAt),
        })
      }
    }

    return coilPlaces
  }

  function toDTO(entity: CoilFarm): CoilFarmDTO {
    return {
      id: entity.id,
      name: entity.name,
      places: {
        buffers: toCoilPlaceDTOs(entity.places.buffers),
        coils: toCoilPlaceDTOs(entity.places.coils),
      },
      type: entity.type,
      updatedBy: entity.updatedBy,
      deletedAt: DATE.toDTO(entity.deletedAt),
      createdAt: DATE.toDTO(entity.createdAt),
      updatedAt: DATE.toDTO(entity.updatedAt),
    }
  }

  function toPartialDTO(entity: Partial<CoilFarm>): Partial<CoilFarmDTO> {
    return {
      ...entity,
      places: {
        buffers: entity.places?.buffers
          ? toCoilPlaceDTOs(entity.places?.buffers)
          : [],
        coils: entity.places?.coils
          ? toCoilPlaceDTOs(entity.places?.coils)
          : [],
      },
      deletedAt: DATE.toDTO(entity.deletedAt),
      createdAt: DATE.toDTO(entity.createdAt),
      updatedAt: DATE.toDTO(entity.updatedAt),
    }
  }
  function fromDTO(entity: CoilFarmDTO): CoilFarm {
    return {
      ...entity,
      places: {
        buffers: entity.places?.buffers
          ? fromCoilPlaceDTOs(entity.places.buffers)
          : [],
        coils: entity.places?.coils
          ? fromCoilPlaceDTOs(entity.places.coils)
          : [],
      },
      deletedAt: DATE.fromDTO(entity.deletedAt),
      createdAt: DATE.fromDTO(entity.createdAt),
      updatedAt: DATE.fromDTO(entity.updatedAt),
    }
  }

  const commonApi = COMMON_API<CoilFarm, CoilFarmDTO>(
    '/module/coilfarm/coilfarms',
    {
      transforms: {
        toDTO,
        toPartialDTO,
        fromDTO,
      },
      targetArray: coilFarmArray.value,
    }
  )

  // for CoilFarmLine
  const addCoilToCoilFarm = Update(
    '/module/coilfarm/coilfarms/{{id}}/coil/{{coilId}}',
    {
      transforms: {
        toDTO,
        toPartialDTO,
        fromDTO,
      },
      targetArray: coilFarmArray.value,
    }
  )

  return {
    ...commonApi,
    coilFarmArray,
    addCoilToCoilFarm,
  }
})