import {defineStore} from "pinia"
import {CoilPlace , CoilPlaceDTO} from "@/types/coilfarm/CoilPlace";
import Get from "@/core/api/Get"
import Update from "@/core/api/Update"
import Remove from "@/core/api/Remove"
import COMMON_API from "@/core/api/CommonApi"
import GetAll from "@/core/helper/date"

export const useCoilPlacesStore = defineStore('Coilplacesstore', () => {
    const coilPlacesArray = ref<CoilPlace[]>([])

    function toDTO(entity: CoilPlace): CoilPlaceDTO {
        return {
            ...entity,
            onPlaceAt: DATE.toDTO(entity.onPlaceAt),
            createdAt: DATE.toDTO(entity.createdAt),
            updatedAt: DATE.toDTO(entity.updatedAt)
        }
    }

    
    function toPartialDTO(entity: Partial<CoilPlace>): Partial<CoilPlaceDTO> {
        return {
            ...entity,
            onPlaceAt: DATE.toDTO(entity.onPlaceAt),
            createdAt: DATE.toDTO(entity.createdAt),
            updatedAt: DATE.toDTO(entity.updatedAt),
        }
    }

    function fromDTO(entity: CoilPlaceDTO):CoilPlace {
        return {
            ...entity,
            onPlaceAt: DATE.fromDTO(entity.onPlaceAt),
            createdAt: DATE.fromDTO(entity.createdAt),
            updatedAt: DATE.fromDTO(entity.updatedAt),
        }
    }

    const commonApi = COMMON_API<CoilPlace, CoilPlaceDTO>(
        '/module/coilfarm/coilplaces',
        {
            transforms: {toDTO, toPartialDTO, fromDTO},
            targetArray: coilPlacesArray.value,
        }
    )

    const getCoilPlacesTemplate = Get('/module/coilfarm/coilplaces/template', {
        transforms: {
            toDTO,
            toPartialDTO,
            fromDTO,
        }
    })


    const updateCoilPlaceById = Update('/module/coilfarm/coilplaces/{{id}}', {
        transforms: {
            toDTO,
            toPartialDTO,
            fromDTO,
        }
    })

    const deleteCoilPlaceById = Remove('/module/coilfarm/coilplaces/{{id}}', [])

    const updateCoilPlaceByIdAndCoilId = Update(
        '/module/coilfarm/coilplaces/{{id}}/coil/{{coilId}}',
        {
            transforms: {
                toDTO,
                toPartialDTO,
                fromDTO,
            },
        }
    )

    const getCoilPlacesFromCoilFarm = GetAll(
        '/module/coilfarm/coilfarms/{{id}}/places',
        {
            transforms: {
                toDTO,
                toPartialDTO,
                fromDTO,
            },
        }
    )

    const deleteCoilFromCoilPlaceById = Remove(
        'module/coilfarm/coilplaces/{{id}}/coil',
        []
    )


    return {
        ...commonApi,
        getCoilPlacesTemplate,
        updateCoilPlaceById,
        deleteCoilPlaceById,
        updateCoilPlaceByIdAndCoilId,
        deleteCoilFromCoilPlaceById,
        coilPlacesArray,
        getCoilPlacesFromCoilFarm,
    }
})