import { defineStore } from "pinia";
import { Config , ConfingDTO } from "@/types/coilfarm/Config";
import Add from "@/core/api/Create"
import Get from "@/core/api/Get"

export const useConfigStore = defineStore('Configstore', () => {
    function toDTO(entity:Config):ConfingDTO {
        return {
            ...entity,
            createdAt: entity.createdAt.toISOString(),
        }
    }

    function toPartialDTO(entity: Partial<Config>): Partial<ConfigDTO> {
        return {
            ...entity,
            createdAt: entity.createdAt?.toISOString(),
        }
    }


    function fromDTO(entity: ConfingDTO):Config {
        return {
            ...entity,
            createdAt:new Date(entity.createdAt)
        }
    }

    const addConfigToCoilFarm = Add('/module/coilfarm/coilfarms/{{id}}/configs', {
        transforms: {
            toDTO,
            toPartialDTO,
            fromDTO,
        },
    })


    const getConfigsFromCoilFarm = Get(
        '/module/coilfarm/coilfarms/{{id}}/configs',
        {
            transforms: {
                toDTO,
                toPartialDTO,
                fromDTO,
            },
        }
    )

    const getActiveConfigsFromCoilFarm = Get(
        '/module/coilfarm/coilfarms/{{id}}/configs/active',
        {
            transforms: {
                toDTO,
                toPartialDTO,
                fromDTO,
            }
        }
    )

    const getConfigByIdFromCoilFarm = Get(
        '/module/coilfarm/coilfarms/{{id}}/configs/{{configId}}',
        {
            transforms: {
                toDTO,
                toPartialDTO,
                fromDTO,
            },
        }
    )

    return {
        addConfigToCoilFarm,
        getConfigsFromCoilFarm,
        getActiveConfigsFromCoilFarm,
        getConfigByIdFromCoilFarm
    }
})