import { defineStore } from "pinia"

export const useStorageStore = defineStore("StorageStore", () => {
    const CLIENT_ID = "nuClientId"
    const USER_TOKEN = "nuUserToken"

    function set(item: { [key:string]: string }) {
        localStorage.setItem(item.key, item.value)
    }

})
