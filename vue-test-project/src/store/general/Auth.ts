import { defineStore} from "pinia"
import { computed, ref, toRaw } from "vue"
import GET, {GET_RAW} from "@/core/api/Get";
import {Auth, AuthDTO} from "@/types/general/auth"
import { useStorageStore} from "@/store/general/Storage"
import UUID from "@/core/helper/uuid"
import { CREATE_RAW } from "@/core/api/Create"
import api from "@/core/api"

export const useAuthStore = defineStore("AuthStore", () => {
    const storage = useStorageStore()
    const permissions = ref<string[]>([])

    const loggedIn = ref(false)
    const isLoggedIn = computed(() => loggedIn.value)

    const loading = ref(false)
    const hasPermission = (permission:string) => {
        computed(() => permissions.value.includes(permission))
    }

    //permissionsToCheck is an array of type string?
    const hasOneOfPermissions = (permissionsToCheck: string[]) => {
        return permissionsToCheck.some((permission) => {
            return hasPermission(permission).value
        })
    }

    //permissionStart is a string
    const hasPermissionStartingWith = (permissionStart: string) => {
        for (const permission of permissions.value) {
            if(permission.startsWith(permissionStart)) {
                return true
            }
        }
        return false
    }

    //domain extension here?
    //take in params ?
    //loginRequest is an object?
    const loginRequest = CREATE_RAW<Auth , AuthDTO>("/core/user/login")
    const login = (email: string, password: string) => {
        loading.value = true;
        return loginRequest({
            email, password
        }, 
        {axiosConfig : {params: {clientId: getClientId() }},
    }
    ).then((user) => {
        return updateAuth(user)
    })
    .finally(() => {
        loading.value = false
    })
    }


    //getting client id and setting clientid when you have none
    function getClientId() {
        let clientId = storage.getClientId()

        if (clientId === null || clientId.length === 0) {
            clientId = UUID.get()
            storage.setClientId(clientId)
        }
    }

    //saw this in the axios documentation using api.defaults
    const logout = () => {
        storage.setUserToken("")
        api.defaults.common["Authorization"];
        loggedIn.value = false
    }

    const updateAuth = function(auth:AuthDTO) {
        if(auth) {
            storage.setUserToken(auth.token);
            api.defaults.common["Authorization"] = 'Bearer' + auth.token;
            loggedIn.value = true;
            permissions.value = auth.permissions.map((entity) => {
                return entity.id
            })
        } else {
            throw new Error("received new is invalid")
        }
    }



    const renewRequest = GET_RAW<AuthDTO>("/core/user/renew")
    const renew = () => {
        loading.value = true;
        api.defaults.common["Authorization"] = 'Bearer' + storage.getUserToken()

        return renewRequest({
            axiosConfig: {
                params: {clientId: getClientId() , token: storage.getUserToken() },
            }
        })
        .then((auth) => {
            return updateAuth(auth)
        })

        .catch(() => {
            throw new Error("renew failed")
        })

        .finally(() => {
            loading.value = false
        })
    }





})

