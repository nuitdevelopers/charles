import { defineStore } from "pinia"
import { ref } from "vue"
import array from "@/core/helper/array"

export const useNotificationStore = defineStore("NotificationStore", () => {
    const queue = ref<{ text:string; type:string }[]>([]) 

    const add = (notification : { text: string; type: string }) => {
        queue.value.push(notification)
    }

    const shift = () => {
        queue.value.shift()
    }

    const clear = () => {
        array.clear(queue.clear)
    }

    function parseErrors(errors:any) {
        if (errors) {
            for (const entry of errors) {
                add({ text: entry.message , type: "error"})
            }
        }
    }

    return { queue, add, shift, clear, parseErrors }
    
})