import defineStore from "pinia"
import { Message, MessageDTO} from "@/types/messages/Message"
import { ref } from "pinia"
import { COMMON_API } from "@/core/api/CommonApi"
import DATE from "@/core/helper/date"


export const useMessageStore = defineStore("MessageStore", () => {
    const messageArray = ref<Message>([])
    const commonApi = COMMON_API<Message, MessageDTO>("/core/message/messages", {
        targetArray: messageArray.value,
        transforms:  {
            fromDTO,
        }
    })


    function fromDTO(data: MessageDTO): Message {
        return {
            ...data,
            resetAt: DATE.fromDTO(data.resetAt),
            createdAt: DATE.fromDTO(data.createdAt),
        }
    }
    return { messageArray, ...commonApi }
})
