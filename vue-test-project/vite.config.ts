import { defineConfig, loadEnv } from 'vite'
import vue from '@vitejs/plugin-vue'
import vueI18n from '@intlify/vite-plugin-vue-i18n'
import viteVuetify from '@vuetify/vite-plugin'

import { join, resolve } from 'path'
export default defineConfig(({ command, mode }) => {
  const env = loadEnv(mode, process.cwd(), '')

  return {
    mode: mode,
    resolve: {
      alias: {
        '@': join(__dirname, './src'),
      },
    },
    plugins: [
      vue(),
      vueI18n({
        include: resolve(__dirname, './locales/**'),
      }),
      viteVuetify({
        autoImport: true,
        styles: 'expose',
      }),
    ],
    define: { __APP_ENV__: env.APP_ENV },
    css: {
      preprocessorOptions: {
        scss: {
          additionalData: ` @use "@/styles/appmain.scss";\n`,
        },
      },
    },
  }
})