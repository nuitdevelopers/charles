const chatForm = document.getElementById("chat-form")
const chatMessages = document.querySelector(".chat-messages")
const roomName = document.getElmentById("room-name")
const userList = document.getElementById("users");


//get username and room frlom URL
const { username, room } = Qs.parse(location.search, {
    ignoreQueryPrefix: true,
})

const socket = io()


//join chat room
socket.emit("joinRoom", { username,room })

//get room and users

socket.on("roomUsers", ({ room , users }) => {
    outputRoomName(room)
    outputUsers(users)
});

//message from server
socket.on("message", (message) => {
    console.log(message)
    outputMessage(message)



    //scroll down
    chatMessages.scrollTop = chatMessages.scrollHeight;
});


//meesage submit
chatForm.addEventListener("submit", (e) => {
    e.preventDefault()

    //get message text
    let msg = e.target.elements.msg.value;

    msg = msg.trim()

    if(!msg) {
        return False
    }


    //emit message to server
    socket.emit("chatMessage", msg)


    //clear input
    e.target.elements.msg.value = "";
    e.target.elements.msg.focus()
})


function outputMessage(users) {
    roomName.innerText = room;
}

//add users to DOM
function outputUsers(users) {
    userList.innerHTML = "";
    users.forEach((user) => {
        const li = document.createElement("li");
        li.innerText = user.username;
        userList.appendChild(li)
    })
}

//prompt the user before leaving the chat room

document.getElementById("leave-btn").addEventListener("click", () => {
    const leaveRoom = confirm("Are you sure you want to leave the chatroom?")
    if(leaveRoom) {
        window.location = "../index.html"
    } else {
        console.log("file not found")
    }
})