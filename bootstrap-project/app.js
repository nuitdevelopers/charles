const path = require("path");
const http = require("http")
const express = require("express")
const socketio = require("socket.io")
const formatMessage = require("./utils/messages")

const { userJoin, getCurrentUser, userLeave , getRoomUsers} = require("./utils/users");


var app = express()
var server = http.createServer(app);
var io = socketio(server)

const port = 3000;

//set static folder

app.use(express.static(path.join(__dirname, "public")))

const botName = "Chat bot"

//run when the client connects
io.on("connection", socket => {
    

})



server.listen(port, () => {
    console.log("server is running!")
})