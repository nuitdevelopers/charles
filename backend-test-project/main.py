from sklearn.model_selection import train_test_split
from sklearn.datasets import fetch_california_housing
from sklearn.preprocessing import StandardScaler
from IPython.display import SVG
import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import os
import pandas as pd
import sklearn
import sys
import tensorflow as tf
from tensorflow import keras  # tf.keras
import time

# print('python', sys.version)
# for module in mpl, np, pd, sklearn, tf, keras:
# print(module.__name__, module.__version__)


assert sys.version_info >= (3, 5)
assert tf.__version__ >= "2.0"

# image classification with keras
fashion_mnist = keras.datasets.fashion_mnist
(x_train_full, y_train_full), (x_test, y_test) = (fashion_mnist.load_data())
x_valid, x_train = x_train_full[:5000], x_train_full[5000:]
y_valid, y_train = y_train_full[:5000], y_train_full[5000:]


# print(x_train.shape)

# print(x_train[0])


# plt.imshow(x_train[1], cmap="binary")

# plt.show()

# print("class id's for y_train: ", y_train)

class_names = ["t-shirt", "trouser", "pullover", "dress",
               "coat", "sandal", "shirt", "sneaker", "bag", "ankle boot"]

# first image in the training set
# print(class_names[y_train[2]])

# print(x_valid.shape)

# print(x_test.shape)

# a sample of the images in the dataset
n_rows = 5
n_cols = 10
# plt.figure(figsize=(n_cols*1.4, n_rows*1.6))
for row in range(n_rows):
    for col in range(n_cols):
        index = n_cols * row + col
        plt.subplot(n_rows, n_cols, index+1)
        plt.imshow(x_train[index], cmap="binary", interpolation="nearest")
        plt.axis("off")
        plt.title(class_names[y_train[index]])
# plt.show()


# Exercise 2

model = keras.models.Sequential()
model.add(keras.layers.Flatten(input_shape=[28, 28]))
model.add(keras.layers.Dense(300, activation="relu"))
model.add(keras.layers.Dense(100, activation="relu"))
model.add(keras.layers.Dense(10, activation="softmax"))


model = keras.models.Sequential([
    keras.layers.Flatten(input_shape=[28, 28]),
    keras.layers.Dense(300, activation="relu"),
    keras.layers.Dense(100, activation="relu"),
    keras.layers.Dense(10, activation="softmax")
])

# print(model.layers)


# print(model.summary())


keras.utils.plot_model(model, "my_mnist_model.png", show_shapes=True)


#SVG(keras.utils.model_to_dot(model, show_shapes=True).create(prog="dot", format="svg"))


model.compile(loss="sparse_categorical_crossentropy", optimizer=keras.optimizers.SGD(
    learning_rate=1e-3), metrics=["accuracy"])


history = model.fit(x_train, y_train, epochs=5,
                    validation_data=(x_valid, y_valid))


def plot_learning_curves(history):
    pd.DataFrame(history.history).plot(figsize=(8, 5))
    plt.grid(True)
    plt.gca().set_ylim(0, 1)
    plt.show()


# print(plot_learning_curves(history))


history = model.fit(x_train, y_train, epochs=5,
                    validation_data=(x_valid, y_valid))

model.evaluate(x_test, y_test)

n_new = 10
x_new = x_test[:n_new]
y_proba = model.predict(x_new)
y_proba.round(2)

# get the most likely class for each instance
y_pred = y_proba.argmax(axis=1)
# print(y_pred)
#

# find the estimated probability for each predicted class
# print(y_proba.max(axis=1).round(2))


# find the top k classes and their estimated probabilites
k = 3
top_k = np.argsort(-y_proba, axis=1)[:, :k]
# print(top_k)

row_indices = np.tile(np.arange(len(top_k)), [k, 1]).T
y_proba[row_indices, top_k].round(2)


# Exercise 3


pixel_means = x_train.mean(axis=0)
pixel_stds = x_train.std(axis=0)
x_train_scaled = (x_train - pixel_means) / pixel_stds
x_valid_scaled = (x_valid - pixel_means) / pixel_stds
x_test_scaled = (x_test - pixel_means) / pixel_stds


scaler = StandardScaler()
x_train_scaled = scaler.fit_transform(x_train.astype(
    np.float32).reshape(-1, 28*28)).reshape(-1, 28 * 28)
x_valid_scaled = scaler.transform(x_valid.astype(
    np.float32).reshape(-1, 28*28)).reshape(-1, 28*28)
x_test_scaled = scaler.transform(x_test.astype(
    np.float32).reshape(-1, 28*28)).reshape(-1, 28*28)


model = keras.models.Sequential([
    keras.layers.Flatten(input_shape=[28, 28]),
    keras.layers.Dense(300, activation="relu"),
    keras.layers.Dense(100, activation="relu"),
    keras.layers.Dense(10, activation="softmax")
])


model.compile(loss="sparse_categorical_crossentropy",
              optimizer=keras.optimizers.SGD(1e-3), metrics=["accuracy"])

history = model.fit(x_train_scaled, y_train, epochs=5,
                    validation_data=(x_valid_scaled, y_valid))

model.evaluate(x_test_scaled, y_test)


plot_learning_curves(history)


# Exercise 4

model = keras.Sequential([
    keras.layers.Flatten(input_shape=[28, 28]),
    keras.layers.Dense(300, activation="relu"),
    keras.layers.Dense(100, activation="relu"),
    keras.layers.Dense(10, activation="softmax")
])

model.compile(loss="sparse_categorical_crossentropy",
              optimizer=keras.optimizers.SGD(1e-3), metrics=["accuracy"])

#logdir = os.path.join(root_logdir, "run_{}".format(time.time()))


callbacks = [
    keras.callbacks.TensorBoard(logdir),
    keras.callbacks.EarlyStopping(patience=5),
    keras.callbacks.ModelCheckpoint("my_mnist_model.h5", save_best_only=True),
]

history = model.fit(x_train_scaled, y_train, epochs=10, validation_data=(
    x_valid_scaled, y_valid), callback=callbacks)


model = model.keras.load_model("my_mnist_model.h5")

model.evaluate(x_valid_scaled, y_valid)
