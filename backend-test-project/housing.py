from sklearn.model_selection import train_test_split
import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import os
import pandas as pd
import sklearn
import sys
import tensorflow as tf
from tensorflow import keras  # tf.keras
import time
from sklearn.datasets import fetch_california_housing

housing = fetch_california_housing()

# print(housing.DESCR)

# print(housing.data.shape)

# print(housing.target.shape)


x_train_full, x_test, y_train_full, y_test
