import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'

const routes = [
  {
    path: '/',
    name: 'home',
    component: HomeView
  },
  {
    path: '/topview',
    name: 'topview',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: function () {
      return import(/* webpackChunkName: "about" */ '../views/Topview.vue')
    }
  },
  {
    path: '/bottomview',
    name: 'bottomview',
    component: function() {
      return import('../views/Bottomview.vue')
    }
  },
  {
    path:'/test',
    name: 'test',
    component: function() {
      return import('../views/Test.vue')
    }
  },
  {
    path: '/messages',
    name:'messages',
    component: function() {
      return import('../views/Messages.vue')
    }
  },
  {
    path: '/coilfarm',
    name: 'coilfarm',
    component: function() {
      return import('../views/Coilfarm.vue')
    }
  },
  {
    path: '/materials',
    name: 'materials',
    component: function() {
      return import('../views/Materials.vue')
    }
  }, 
  {
    path: '/test2',
    name: 'test2',
    component: function() {
      return import('../views/Test2.vue')
    }
  },
  {
    path: '/test',
    name: 'test',
    component: function() {
      return import('../views/Test.vue')
    }
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
